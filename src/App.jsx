import React from "react";
import "./App.scss";
import { useState } from "react";
import { Hero, About, Education, Experience, More } from "./components/index";
import { CV } from "./CV/CV";

const { hero, education, experience, languages, habilities, volunteer } = CV;

function App() {
  const [showEducation, setShowEducation] = useState(true);
  return (
    <>
      <div>
        <Hero hero={hero}></Hero>
        <About hero={hero}></About>
        <Education education={education}></Education>
        <Experience experience={experience}></Experience>
        <button
        className="custom-btn btn-4"
        onClick={() => setShowEducation(true)}
      >
        Education
      </button>
      <button
        className="custom-btn btn-4"
        onClick={() => setShowEducation(false)}
      >
        Experience
      </button>

      <div>
        {showEducation ? (
          <Education education={education} />
        ) : (
          <Experience experience={experience} />
        )}
      </div>
        <More
          languages={languages}
          habilities={habilities}
          volunteer={volunteer}
        />
      </div>
    </>
  );
}

export default App;
