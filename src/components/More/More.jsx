import React from "react";
import "./More.scss";

const More = (items) => {
  return (
    <div className="hero">
      <div className="education card">
        <h4>Languages</h4>
        <div>
          <p className="name">{items.languages.language}</p>
          <p className="name">🖋 Writing: {items.languages.wrlevel}</p>
          <p className="name">📣 Speaking: {items.languages.splevel}</p>
        </div>
      </div>
      <div className="education card">
        <h4>Skills</h4>
        {items.habilities.map((item) => {
          return (
            <div key={JSON.stringify(item)}>
              <p className="name">🔧{item}</p>
            </div>
          );
        })}
      </div>
      <div className="education card">
        <h4>Volunteer</h4>
        {items.volunteer.map((item) => {
          return (
            <div key={JSON.stringify(item)}>
              <p className="name">🥽{item.name}</p>
              <p>{item.where}</p>
              <p>{item.description}</p>
            </div>
          );
        })}
      </div>
    </div>
  );
};

export default More;
