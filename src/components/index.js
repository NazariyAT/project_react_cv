import Hero from './Hero/Hero'
import Experience from './Experience/Experience'
import Education from './Education/Education'
import About from './About/About'
import More from './More/More'


export {Hero,Experience,Education,About,More }