import React from "react";
import "./About.scss";

const About = ({ hero }) => {
  return (
    <div className="hero">
    <h4>About Me</h4>
      <div className="education card">
        {hero.aboutMe.map((item) => {
          return (
            <div key={JSON.stringify(item)}>
              <p className="name">{item.info}</p>
            </div>
          );
        })}
      </div>
    </div>
  );
};

export default About;
